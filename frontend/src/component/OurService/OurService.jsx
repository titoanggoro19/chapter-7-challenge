import React from "react";

const OurService = () => {
  return (
    <section className="ourservice-section" id="ourservice-section">
      <div className="container">
        <div className="row">
          <div className="col-sm-12 col-md-12 col-lg-6 col-xxl-6 text-center text-xxl-start img-ourservice">
            <img
              className="img-ourservice img-fluid"
              src="Images/img_service.png"
              alt=""
            />
          </div>
          <div className="col-sm-12 col-md-12 col-lg-6 col-xxl-6">
            <h1>Best Car Rental for any kind of trip in (Lokasimu)!</h1>
            <p>
              Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga
              lebih murah dibandingkan yang lain, kondisi mobil baru, serta
              kualitas pelayanan terbaik untuk perjalanan wisata, bisnis,
              wedding, meeting, dll.
            </p>
            <div>
              <ul>
                <li>
                  <img src="Images/ceklist.png" alt="" />
                  <span>Sewa Mobil Dengan Supir di Bali 12 Jam</span>
                </li>
                <li>
                  <img src="Images/ceklist.png" alt="" />
                  <span>Sewa Mobil Lepas Kunci di Bali 24 Jam</span>
                </li>
                <li>
                  <img src="Images/ceklist.png" alt="" />
                  <span>Sewa Mobil Jangka Panjang Bulanan</span>
                </li>
                <li>
                  <img src="Images/ceklist.png" alt="" />
                  <span>Gratis Antar - Jemput Mobil di Bandara</span>
                </li>
                <li>
                  <img src="Images/ceklist.png" alt="" />
                  <span>Layanan Airport Transfer / Drop In Out</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default OurService;
