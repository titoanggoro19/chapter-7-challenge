import { useEffect, useState } from "react";
import SearchCarFilter from "./SearchCarFilter";
import { useSelector, useDispatch } from "react-redux";
import { fetchUsers } from "../../redux";

const SearchCar = () => {
  const [type, setType] = useState("Pilih Tipe Driver");
  const [date, setDate] = useState("Pilih Waktu");
  const [pickupTime, setPickupTime] = useState("8");
  const [passenger, setPassenger] = useState("");
  const dispatch = useDispatch();
  const state = useSelector((state) => state);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (type !== "Pilih Tipe Driver") {
      const pass = passenger ? passenger : "0";
      const filter = { type, date, pickupTime, pass };
      dispatch(fetchUsers(filter));
    }
  };

  const handleType = (e) => {
    setType(e.target.value);
  };
  const handleDate = (e) => {
    setDate(e.target.value);
  };
  const handlePickupTime = (e) => {
    setPickupTime(e.target.value);
  };
  const handlePassenger = (e) => {
    setPassenger(e.target.value);
  };

  useEffect(() => {
    console.log(state.cars);
  }, [state]);

  return (
    <>
      <div className="section-car-section">
        <form onSubmit={handleSubmit}>
          <div
            id="searchColumn"
            className="container d-flex justify-content-center"
          >
            <div
              className="row d-flex justify-content-center search-car"
              style={{ zIndex: 2 }}
          
            >
              <div className="col-xxl-2 col-sm-5">
                <div className="mb-3 mt-2">
                  <label htmlFor="typeDriver">Tipe Driver</label>
                  <select
                    value={type}
                    required
                    onChange={handleType}
                    className="form-select type"
                    name="typeDriver"
                    id="typeDriver"
                  >
                    <option disabled hidden>
                      Pilih Tipe Driver
                    </option>
                    <option value="Dengan Sopir">Dengan Sopir</option>
                    <option value="Keyless Entry">
                      Tanpa Sopir (Lepas Kunci)
                    </option>
                  </select>
                </div>
              </div>
              <div className="col-xxl-2 col-sm-5">
                <div className="mb-3 mt-2">
                  <label htmlFor="date">Pilih Tanggal</label>
                  <input
                    className="form-control"
                    onChange={handleDate}
                    required
                    type="date"
                    name="date"
                    id="date"
                  />
                </div>
              </div>
              <div className="col-xxl-2 col-sm-5">
                <div className="mb-3 mt-2">
                  <label htmlFor="time">Waktu Ambil</label>
                  <select
                    value={pickupTime}
                    onChange={handlePickupTime}
                    className="form-select time"
                    name="time"
                    id="time"
                  >
                    <option value="8">08.00 WIB</option>
                    <option value="9">09.00 WIB</option>
                    <option value="10">10.00 WIB</option>
                    <option value="11">11.00 WIB</option>
                    <option value="12">12.00 WIB</option>
                  </select>
                </div>
              </div>
              <div className="col-xxl-2 col-sm-5">
                <div className="mb-3 mt-2">
                  <label htmlFor="passenger">Penumpang</label>
                  <input
                    value={passenger}
                    onChange={handlePassenger}
                    className="form-control"
                    type="text"
                    name="passenger"
                    id="passenger"
                  />
                </div>
              </div>
              <div className="col-xxl-auto col-sm-5 text-center mt-xxl-2 mt-md-0">
                <div className="mb-3 mt-xxl-4">
                  <button className="btn btn-success btn-green-custom submit">
                    Pilih Mobil
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      {state.cars && <SearchCarFilter cars={state.cars} />}
    </>
  );
};

export default SearchCar;
