import React from "react";

const Faq = () => {
  return (
    <section className="FAQ-section" id="faq-section">
      <div className="container">
        <div className="row FAQ-content text-center">
          {/* TItle FAQ */}
          <div className="col-sm-6 col-md-12 col-lg-6 col faq-desc-content text-lg-start">
            <h1>Frequently Asked Question</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          </div>
          {/* TItle FAQ */}
          {/* Accordion FAQ */}
          <div className="col-sm-6 col-md-12 col-lg-6 colfaq-list-content">
            <div className="accordion" id="accordionPanelsStayOpenExample">
              <div className="accordion-item">
                <h2 className="accordion-header" id="panelsStayOpen-headingOne">
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseOne"
                    aria-expanded="false"
                    aria-controls="panelsStayOpen-collapseOne"
                  >
                    Apa saja syarat yang dibutuhkan
                  </button>
                </h2>
                <div
                  id="panelsStayOpen-collapseOne"
                  className="accordion-collapse collapse"
                  aria-labelledby="panelsStayOpen-headingOne"
                >
                  <div className="accordion-body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Repellendus qui vitae quas aut architecto amet, sit rem
                    nesciunt, eveniet magnam animi nobis obcaecati commodi
                    dolore. Aspernatur perspiciatis quidem quae vero! Adipisci
                    voluptate doloribus magni eaque labore facilis saepe
                    voluptatibus ab officia, ipsam suscipit, possimus totam.
                    Rerum doloremque laborum, pariatur praesentium officiis
                    nobis, illum quisquam quis, consectetur accusantium adipisci
                    exercitationem nostrum. Commodi iusto beatae, corporis sunt
                    itaque fugiat quibusdam cumque! Eligendi, enim odio sapiente
                    asperiores eos magnam pariatur quibusdam itaque, eius nam
                    perspiciatis explicabo. Laudantium voluptatibus provident
                    similique neque, at voluptates.
                  </div>
                </div>
              </div>
              <div
                className="accordion-item"
                style={{ border: "1px solid #d4d4d4" }}
              >
                <h2 className="accordion-header" id="panelsStayOpen-headingTwo">
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseTwo"
                    aria-expanded="false"
                    aria-controls="panelsStayOpen-collapseTwo"
                  >
                    Berapa hari minimal sewa mobil lepas kunci?
                  </button>
                </h2>
                <div
                  id="panelsStayOpen-collapseTwo"
                  className="accordion-collapse collapse"
                  aria-labelledby="panelsStayOpen-headingTwo"
                >
                  <div className="accordion-body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Repellendus qui vitae quas aut architecto amet, sit rem
                    nesciunt, eveniet magnam animi nobis obcaecati commodi
                    dolore. Aspernatur perspiciatis quidem quae vero! Adipisci
                    voluptate doloribus magni eaque labore facilis saepe
                    voluptatibus ab officia, ipsam suscipit, possimus totam.
                    Rerum doloremque laborum, pariatur praesentium officiis
                    nobis, illum quisquam quis, consectetur accusantium adipisci
                    exercitationem nostrum. Commodi iusto beatae, corporis sunt
                    itaque fugiat quibusdam cumque! Eligendi, enim odio sapiente
                    asperiores eos magnam pariatur quibusdam itaque, eius nam
                    perspiciatis explicabo. Laudantium voluptatibus provident
                    similique neque, at voluptates.
                  </div>
                </div>
              </div>
              <div
                className="accordion-item"
                style={{ border: "1px solid #d4d4d4" }}
              >
                <h2
                  className="accordion-header"
                  id="panelsStayOpen-headingThree"
                >
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseThree"
                    aria-expanded="false"
                    aria-controls="panelsStayOpen-collapseThree"
                  >
                    Berapa hari sebelumnya sabaiknya booking sewa mobil?
                  </button>
                </h2>
                <div
                  id="panelsStayOpen-collapseThree"
                  className="accordion-collapse collapse"
                  aria-labelledby="panelsStayOpen-headingThree"
                >
                  <div className="accordion-body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Repellendus qui vitae quas aut architecto amet, sit rem
                    nesciunt, eveniet magnam animi nobis obcaecati commodi
                    dolore. Aspernatur perspiciatis quidem quae vero! Adipisci
                    voluptate doloribus magni eaque labore facilis saepe
                    voluptatibus ab officia, ipsam suscipit, possimus totam.
                    Rerum doloremque laborum, pariatur praesentium officiis
                    nobis, illum quisquam quis, consectetur accusantium adipisci
                    exercitationem nostrum. Commodi iusto beatae, corporis sunt
                    itaque fugiat quibusdam cumque! Eligendi, enim odio sapiente
                    asperiores eos magnam pariatur quibusdam itaque, eius nam
                    perspiciatis explicabo. Laudantium voluptatibus provident
                    similique neque, at voluptates.
                  </div>
                </div>
              </div>
              <div
                className="accordion-item"
                style={{ border: "1px solid #d4d4d4" }}
              >
                <h2
                  className="accordion-header"
                  id="panelsStayOpen-headingFour"
                >
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseFour"
                    aria-expanded="false"
                    aria-controls="panelsStayOpen-collapseFour"
                  >
                    Apakah Ada biaya antar-jemput?
                  </button>
                </h2>
                <div
                  id="panelsStayOpen-collapseFour"
                  className="accordion-collapse collapse"
                  aria-labelledby="panelsStayOpen-headingFour"
                >
                  <div className="accordion-body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Repellendus qui vitae quas aut architecto amet, sit rem
                    nesciunt, eveniet magnam animi nobis obcaecati commodi
                    dolore. Aspernatur perspiciatis quidem quae vero! Adipisci
                    voluptate doloribus magni eaque labore facilis saepe
                    voluptatibus ab officia, ipsam suscipit, possimus totam.
                    Rerum doloremque laborum, pariatur praesentium officiis
                    nobis, illum quisquam quis, consectetur accusantium adipisci
                    exercitationem nostrum. Commodi iusto beatae, corporis sunt
                    itaque fugiat quibusdam cumque! Eligendi, enim odio sapiente
                    asperiores eos magnam pariatur quibusdam itaque, eius nam
                    perspiciatis explicabo. Laudantium voluptatibus provident
                    similique neque, at voluptates.
                  </div>
                </div>
              </div>
              <div
                className="accordion-item"
                style={{ border: "1px solid #d4d4d4" }}
              >
                <h2
                  className="accordion-header"
                  id="panelsStayOpen-headingFive"
                >
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#panelsStayOpen-collapseFive"
                    aria-expanded="false"
                    aria-controls="panelsStayOpen-collapseFive"
                  >
                    Bagaimana jika terjadi kecelakaan
                  </button>
                </h2>
                <div
                  id="panelsStayOpen-collapseFive"
                  className="accordion-collapse collapse"
                  aria-labelledby="panelsStayOpen-headingFive"
                >
                  <div className="accordion-body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Repellendus qui vitae quas aut architecto amet, sit rem
                    nesciunt, eveniet magnam animi nobis obcaecati commodi
                    dolore. Aspernatur perspiciatis quidem quae vero! Adipisci
                    voluptate doloribus magni eaque labore facilis saepe
                    voluptatibus ab officia, ipsam suscipit, possimus totam.
                    Rerum doloremque laborum, pariatur praesentium officiis
                    nobis, illum quisquam quis, consectetur accusantium adipisci
                    exercitationem nostrum. Commodi iusto beatae, corporis sunt
                    itaque fugiat quibusdam cumque! Eligendi, enim odio sapiente
                    asperiores eos magnam pariatur quibusdam itaque, eius nam
                    perspiciatis explicabo. Laudantium voluptatibus provident
                    similique neque, at voluptates.
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Accordion FAQ */}
        </div>
      </div>
    </section>
  );
};

export default Faq;
