import { BrowserRouter, Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";

import { Provider } from "react-redux";
import store from "./redux/store";

import "./App.css";
import LandingPage from "./pages/LandingPage";
import CarsPage from "./pages/CarsPage";

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<LandingPage />}></Route>
          <Route path="/cars" element={<CarsPage />}></Route>
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
